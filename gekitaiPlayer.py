import threading
from tkinter import *
from tkinter import simpledialog
from click import command

import grpc

import proto.chat_pb2 as chat
import proto.chat_pb2_grpc as rpc

address = 'localhost'
port = 11912

class Player:
    def __init__(self, u: str, window, frameButtons, frameCanvas, vsb, canvas):
        # configurações do jogo
        self.arrayButtons = []

        # o quadro para colocar os componentes da interface do usuário
        self.window = window
        self.frameButtons = frameButtons
        self.frameCanvas = frameCanvas
        self.canvas = canvas
        self.vsb = vsb
        self.username = u
        # criar um canal gRPC + stub
        channel = grpc.insecure_channel(address + ':' + str(port))
        self.conn = rpc.ChatServerStub(channel)

        self.chances = 8

        self.my_id = ""
        # crie um novo thread de escuta para quando novos fluxos de mensagens chegarem
        threading.Thread(target=self.__listen_for_messages, daemon=True).start()
        self.__setup_ui()
        self.createButtons()
        self.window.mainloop()

    def __listen_for_messages(self):
        """
        Este método será executado em um encadeamento separado como o encadeamento principal/ui, porque a chamada for-in está bloqueando
         ao aguardar novas mensagens
        """
        for note in self.conn.ChatStream(chat.Empty()):  # esta linha irá aguardar novas mensagens do servidor!
            splittedText = note.message.split(':')
            if (len(splittedText) > 2):
                if (splittedText[0] == "SETMYID"):
                    if (self.my_id == ""):
                        self.my_id = "X"
                else:
                    buttonValue = splittedText[len(splittedText) - 1]
                    print(self.my_id)
                    if self.my_id == "O":
                        self.b_click(int(buttonValue), 'X')
                    else:
                        self.b_click(int(buttonValue), 'O')
            else:
                print("R[{}] {}".format(note.name, note.message))  # declaração de depuração
                self.chat_list.insert(END, "[{}] {}\n".format(note.name, note.message))  # adicionar a mensagem à interface do usuário

    def send_message(self):
        """
        Este método é chamado quando o usuário digita algo na caixa de texto
        """
        message = self.entry_message.get()  # recuperar mensagem da interface do usuário    
        if message != '':
            n = chat.Note()  # criar mensagem protobuf (chamada Nota)
            n.name = self.username  # definir o nome de usuário
            n.message = message  # definir a mensagem real da nota
            print("S[{}] {}".format(n.name, n.message))  # declaração de depuração
            self.conn.SendNote(n)  # enviar a nota para o servidor

    def __setup_ui(self):
        self.lbl_username = Label(self.window, text=self.username, fg="black")
        self.lbl_username.grid(row=0, column=0, pady=(5, 0), sticky='nw')
        self.entry_message = Entry(self.window, bd=5)
        self.send_button = Button(self.window, text="Enviar Mensagem", font=("Helvetica", 10), command=self.send_message)
        self.send_button.grid(row=0, column=0, pady=(65, 0), sticky='nw')
        self.quit_button = Button(self.window, text="Desistir", font=("Helvetica", 10), command=self.gameQuit)
        self.quit_button.grid(row=0, column=0, pady=(105, 0), sticky='nw')
        self.entry_message.bind('<Return>', self.send_message)
        self.entry_message.focus()
        self.entry_message.grid(row=0, column=0, pady=(25, 0), sticky='nw')
        self.chat_list = Text(self.window)
        self.chat_list.grid(row=3, column=0, pady=5, sticky='nw')

    def createButtons(self):
        rowButton = 0
        colButton = 0
        # LOOP TO CREATE THE BUTTONS
        self.arrayButtons = [Button() for i in range(36)]
        for i in range(36):
            self.arrayButtons[i] = Button(self.frameButtons, text=" ", font=("Helvetica", 20), height=2, width=3, bg="White")
            self.arrayButtons[i]["command"] = lambda i=i: self.b_click(i)
            self.arrayButtons[i].grid(row=rowButton, column=colButton, sticky='news')

            colButton += 1
            if (i in [5, 11, 17, 23, 29, 35]):
                rowButton += 1
                colButton = 0

        # Update buttons frames idle tasks to let tkinter calculate buttons sizes
        self.frameButtons.update_idletasks()

        # Resize the canvas frame to show exactly 5-by-5 buttons and the scrollbar
        first6columns_width = sum([self.arrayButtons[j].winfo_width() for j in range(0, 6)])
        first6rows_height = sum([self.arrayButtons[i].winfo_height() for i in range(0, 6)])
        self.frameCanvas.config(width=first6columns_width + self.vsb.winfo_width(),
                            height=first6rows_height)

        # Set the canvas scrolling region
        self.canvas.config(scrollregion=self.canvas.bbox("all"))

    def checkEquality(self, button, neighbour):
        equal = False

        if neighbour["text"] != " ":
            if neighbour["text"] == button["text"]:
                equal = True

        return equal

    # FUNCTION TO CHECK WIN
    def checkWin(self, index):
        button = self.arrayButtons[index]
        diagonalLeftRight = [[5, -5], [5, 10], [-5, -10]]
        diagonalRightLeft = [[7, -7], [7, 14], [-7, -14]]
        horizontaline     = [[-1, 1], [1, 2], [-1, -2]]
        verticalLine      = [[6, -6], [6, 12], [-6, -12]]

        for possibilities in [diagonalLeftRight, diagonalRightLeft, horizontaline, verticalLine]:
            for i in possibilities:
                countEquality = 0
                for j in i:
                    neighbourIndex = index + j
                    if (neighbourIndex < 0 or neighbourIndex > 34):
                        continue

                    neighbour = self.arrayButtons[neighbourIndex]
                    winner = self.checkEquality(button, neighbour)
                    if winner == True:
                        countEquality += 1
                
                if countEquality == 2:
                    return True
        return False

    # FUNCTION TO MOVE NEIGHBOURS
    def moveNeighbour(self, button, grid):
        skipTreatment = False
        neighbourButton = self.arrayButtons[grid]

        if (grid < 0 or grid > 35):
            skipTreatment = True

        if skipTreatment == False:
            if (neighbourButton["text"] != " "):
                return
            neighbourButton["text"] = button["text"]

        button["text"] = " "

    def treatNeighbours(self, i):
        arrayNeighbours = []
        if (i == 0):
            arrayIndexes = [1, 6, 7]
        elif (i == 5):
            arrayIndexes = [1, 6, 5]
        elif (i in [1,2,3,4]):
            arrayIndexes = [-1, 1, 5, 6, 7]
        elif (i == 30):
            arrayIndexes = [1, -5, -6]
        elif (i in [31, 32, 33, 34]):
            arrayIndexes = [-1, 1, -5, -6, -7]
        elif (i == 35):
            arrayIndexes = [-1, -6, -7]
        else:
            arrayIndexes = [-1, 1, 5, -5, 6, -6, 7, -7]
        
        for j in arrayIndexes:
            neighbourButton = self.arrayButtons[i + j]

            if (neighbourButton["text"] != " "):
                self.moveNeighbour(neighbourButton, i + (j*2))

    # button click function
    def b_click(self, button, symbol = False):
        if (symbol == False):
            symbol = self.my_id
        buttonIndex = button
        button = self.arrayButtons[button]
        if (button["text"] == " "): 
            self.chances -= 1
            
            # ENVIA O ID (QUAL PLAYER ELE E)
            if self.my_id == "":
                self.my_id = "O"
                symbol = self.my_id
                messageToClient = "SETMYID::O"
                n = chat.Note()  # criar mensagem protobuf (chamada Nota)
                n.name = self.username  # definir o nome de usuário
                n.message = messageToClient  # definir a mensagem real da nota
                print("S[{}] {}".format(n.name, n.message))  # declaração de depuração
                self.conn.SendNote(n)  # enviar a nota para o servidor

            # ENVIA O MOVIMENTO PARA O SERVIDOR
            messageToServer = "MOVEBUTTON::" + str(buttonIndex)
            n = chat.Note()  # criar mensagem protobuf (chamada Nota)
            n.name = self.username  # definir o nome de usuário
            n.message = messageToServer  # definir a mensagem real da nota
            print("S[{}] {}".format(n.name, n.message))  # declaração de depuração
            self.conn.SendNote(n)  # enviar a nota para o servidor


            self.treatNeighbours(buttonIndex)
            button["text"] = symbol
            if (self.checkWin(buttonIndex) == True):
                print("Voce Venceu!!")
                exit()
            elif (self.chances == 0):
                print("Chances Esgotadas!!")
                exit()
        pass

    def gameQuit(self):
        print("Voce Desistiu!")
        n = chat.Note()  # criar mensagem protobuf (chamada Nota)
        n.name = self.username  # definir o nome de usuário
        n.message = "Desistencia"  # definir a mensagem real da nota
        self.conn.SendNote(n)  # enviar a nota para o servidor
        exit()

if __name__ == '__main__':
    root = Tk()
    root.grid_rowconfigure(0, weight=1)
    root.columnconfigure(0, weight=1)

    frame_main = Frame(root, bg="gray")
    frame_main.grid(sticky='news')

    # Create a frame for the canvas with non-zero row&column weights
    frame_canvas = Frame(frame_main)
    frame_canvas.grid(row=2, column=0, pady=(5, 0), sticky='nw')
    frame_canvas.grid_rowconfigure(0, weight=1)
    frame_canvas.grid_columnconfigure(0, weight=1)
    # Set grid_propagate to False to allow 5-by-5 buttons resizing later
    frame_canvas.grid_propagate(False)

    # Add a canvas in that frame
    canvas = Canvas(frame_canvas, bg="yellow")
    canvas.grid(row=0, column=0, sticky="news")

    # Link a scrollbar to the canvas
    vsb = Scrollbar(frame_canvas, orient="vertical", command=canvas.yview)
    vsb.grid(row=0, column=1, sticky='ns')
    canvas.configure(yscrollcommand=vsb.set)

    # Create a frame to contain the buttons
    frame_buttons = Frame(canvas, bg="blue")
    canvas.create_window((0, 0), window=frame_buttons, anchor='nw')
    
    root.withdraw()

    username = None
    while username is None:
        # recuperar um nome de usuário para que possamos distinguir todos os diferentes clientes
        username = simpledialog.askstring("Username", "What's your username?", parent=root)
    root.deiconify()  # não lembro mais porque isso era necessário...
    c = Player(username, frame_main, frame_buttons, frame_canvas, vsb, canvas)  # isso inicia um cliente e, portanto, um thread que mantém a conexão com o servidor aberta