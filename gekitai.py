from tkinter import *

root = Tk()
root.title('Gekitai')

def checkEquality(button, neighbour):
    equal = False

    if neighbour["text"] != " ":
        if neighbour["text"] == button["text"]:
            equal = True

    return equal

# FUNCTION TO CHECK WIN
def checkWin(index):
    button = arrayButtons[index]
    diagonalLeftRight = [[5, -5], [5, 10], [-5, -10]]
    diagonalRightLeft = [[7, -7], [7, 14], [-7, -14]]
    horizontaline     = [[-1, 1], [1, 2], [-1, -2]]
    verticalLine      = [[6, -6], [6, 12], [-6, -12]]

    for possibilities in [diagonalLeftRight, diagonalRightLeft, horizontaline, verticalLine]:
        for i in possibilities:
            countEquality = 0
            for j in i:
                neighbourIndex = index + j
                if (neighbourIndex < 0 or neighbourIndex > 34):
                    continue

                neighbour = arrayButtons[neighbourIndex]
                winner = checkEquality(button, neighbour)
                if winner == True:
                    countEquality += 1
            
            if countEquality == 2:
                return True
    return False

# FUNCTION TO MOVE NEIGHBOURS
def moveNeighbour(button, grid):
    skipTreatment = False
    neighbourButton = arrayButtons[grid]

    if (grid < 0 or grid > 35):
        skipTreatment = True

    if skipTreatment == False:
        if (neighbourButton["text"] != " "):
            return
        neighbourButton["text"] = button["text"]

    button["text"] = " "

def treatNeighbours(i):
    arrayNeighbours = []
    if (i == 0):
        arrayIndexes = [1, 6, 7]
    elif (i == 5):
        arrayIndexes = [1, 6, 5]
    elif (i in [1,2,3,4]):
        arrayIndexes = [-1, 1, 5, 6, 7]
    elif (i == 30):
        arrayIndexes = [1, -5, -6]
    elif (i in [31, 32, 33, 34]):
        arrayIndexes = [-1, 1, -5, -6, -7]
    elif (i == 35):
        arrayIndexes = [-1, -6, -7]
    else:
        arrayIndexes = [-1, 1, 5, -5, 6, -6, 7, -7]
    
    for j in arrayIndexes:
        neighbourButton = arrayButtons[i + j]

        if (neighbourButton["text"] != " "):
            moveNeighbour(neighbourButton, i + (j*2))

# button click function
def b_click(button):
    global textTurn
    buttonIndex = button
    button = arrayButtons[button]
    if (button["text"] == " "):
        treatNeighbours(buttonIndex)
        button["text"] = textTurn
        if textTurn == "O":
            textTurn = "X"
        else:
            textTurn = 'O'
        if (checkWin(buttonIndex) == True):
            exit()
    pass

arrayButtons = []
rowButton = 0
colButton = 0
textTurn  = 'O'
# LOOP TO CREATE THE BUTTONS
for i in range(36):
    arrayButtons.append(Button(root, text=" ", font=("Helvetica", 20), height=3, width=6, bg="White"))
    arrayButtons[i]["command"] = lambda i=i: b_click(i)
    arrayButtons[i].grid(row=rowButton, column=colButton)

    colButton += 1
    if (i in [5, 11, 17, 23, 29, 35]):
        rowButton += 1
        colButton = 0


root.mainloop()